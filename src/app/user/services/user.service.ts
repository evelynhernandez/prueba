import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserList, User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  base: string;


  httpOptions = {
    headers: new HttpHeaders({
      'x-api-key': localStorage.getItem('token')
    })
  }

  constructor(private http: HttpClient) {
    this.base = environment.api.apiUrl + '/' + environment.api.users.base;
  }

  getAllUsers(): Observable<UserList> {
    return this.http.get<UserList>(this.base, this.httpOptions);
  }

  newUser(user: User): any {
    return this.http.post(this.base, this.httpOptions);
  }

}
