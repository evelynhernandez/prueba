import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { User } from '../../models/user';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent {

  constructor(private userService: UserService, private router: Router) {
  }

  saveUser(form) {
    var user = new User();
    user.firstName = form.firstName;
    user.lastName = form.lastName;
    user.email = form.email;

    this.userService.newUser(user).subscribe(response => {
      alert("User Created!");
    });
  }

}
