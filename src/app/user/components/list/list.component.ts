import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  users: User[];

  ngOnInit() {

    this.userService.getAllUsers().subscribe(response => {
      this.users = response.data.users;
    });

  }

  public newUser() {
    this.router.navigate(['/user/add']);
  }

}
