export class UserList {
  data: Data;
}

export class Data {
  users: User[];
}

export class User {
  _id: string;
  updated_at: Date;
  created_at: Date;
  email: string;
  firstName: string;
  lastName: string;
  role: string;
  deletedAt?: any;
}





